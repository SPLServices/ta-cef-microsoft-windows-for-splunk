#
# ################################################################################
#
#  Copyright (C) 2018 Splunk Inc.
#
#  You should have received a copy of the Splunk Software Licese agreement
#  along with TA-cef-microsoft-windows-for-splunk.  If not, see:
#  <http://www.splunk.com/en_us/legal/splunk-software-license-agreement.html>
#
# #################################################################################
#
[cef_wineventlog_security]
search = source=CEFEventLog:Security 
#tags = os windows

## Authentication Ticket Granted/Failed
## EventCodes 4768, 4772, 672, 676
[cef_windows_auth_ticket_granted]
search = eventtype=cef_wineventlog_security (EventCode=4768 OR EventCode=672 OR EventCode=676)
#tags = authentication

## Service Ticket Granted/Failed
## EventCodes 4769, 4773, 673, 677
[cef_windows_service_ticket_granted]
search = eventtype=cef_wineventlog_security (EventCode=4769 OR EventCode=4773 OR EventCode=673 OR EventCode=677)
#tags = authentication

## Ticket Granted Renewed
## EventCodes 4770, 674
[cef_windows_ticket_renewed]
search = eventtype=cef_wineventlog_security (EventCode=4770 OR EventCode=674)
## tags intentionally left blank
#tags =

## Pre-authentication failed
## EventCodes 4771, 675
[cef_windows_pre_auth_failed]
search = eventtype=cef_wineventlog_security (EventCode=4771 OR EventCode=675)
#tags = authentication

## Account Mapped for Logon by
## EventCodes 4774, 678
[cef_windows_account_mapped]
search = eventtype=cef_wineventlog_security (EventCode=4774 OR EventCode=678)
## tags intentionally left blank
#tags = authentication

## The name: %2 could not be mapped for logon by: %1
## EventCodes 4775, 679
[cef_windows_account_notmapped]
search = eventtype=cef_wineventlog_security (EventCode=4775 OR EventCode=679)
#tags = authentication

## Account Used for Logon by
## The domain controller attempted/failed to validate the credentials for an account
## The logon to account: %2 by: %1 from workstation: %3 failed.
## EventCodes 4776, 4777, 680, 681
[cef_windows_account_used4logon]
search = eventtype=cef_wineventlog_security (EventCode=4776 OR EventCode=4777 OR EventCode=680 OR EventCode=681)
#tags = authentication

## Session reconnected to winstation
## EventCodes 4778, 682
[cef_windows_session_reconnected]
search = eventtype=cef_wineventlog_security (EventCode=4778 OR EventCode=682)
## tags intentionally left blank
#tags =

## Session disconnected from winstation
## EventCodes 4779, 683
[cef_windows_session_disconnected]
search = eventtype=cef_wineventlog_security (EventCode=4779 OR EventCode=683)
#tags = access stop logoff


###### Security: Account Management ######
[cef_windows_account_management]
search = eventtype=cef_wineventlog_security (ta_windows_security_CategoryString="Account Management" OR TaskCategory="User Account Management")
#tags = account change management

## User/Computer Account Created
## EventCodes 4720, 4741, 624, 645
[cef_windows_account_created]
search = eventtype=cef_wineventlog_security (EventCode=4720 OR EventCode=4741 OR EventCode=624 OR EventCode=645)
#tags = add account change


## User Account Enabled
## EventCodes 4722, 626
[cef_windows_account_enabled]
search = eventtype=cef_wineventlog_security (EventCode=4722 OR EventCode=626)
#tags = enable account change

## Change Password Attempt
## EventCodes 4723, 627
[cef_windows_account_password_change]
search = eventtype=cef_wineventlog_security (EventCode=4723 OR EventCode=627)
#tags = password modify account change

## User Account password set
## EventCodes 4724, 628
[cef_windows_account_password_set]
search = eventtype=cef_wineventlog_security (EventCode=4724 OR EventCode=628)
#tags = password modify account change

## User Account Disabled
## EventCodes 4725, 629
[cef_windows_account_disabled]
search = eventtype=cef_wineventlog_security (EventCode=4725 OR EventCode=629)
#tags = disable account change

## User/Computer Account Deleted
## EventCodes 4726, 4743, 630, 647
[cef_windows_account_deleted]
search = eventtype=cef_wineventlog_security (EventCode=4726 OR EventCode=4743 OR EventCode=630 OR EventCode=647)
#tags = delete account change

## User/Computer Account Changed
## EventCodes 4738, 4742, 642, 646, 625
[cef_windows_account_modified]
search = eventtype=cef_wineventlog_security (EventCode=4738 OR EventCode=4742 OR EventCode=642 OR EventCode=646 OR EventCode=625)
#tags = modify account change

## User Account Locked Out
## EventCodes 4740, 644
[cef_windows_account_lockout]
search = eventtype=cef_wineventlog_security (EventCode=4740 OR EventCode=644)
#tags = lock lockout account change

## User Account Unlocked
## EventCodes 4767, 671
[cef_windows_account_unlocked]
search = eventtype=cef_wineventlog_security (EventCode=4767 OR EventCode=671)
#tags = modify account change


###### Security: Audit (Event Log) ######

## The event logging service has shut down
## EventCode 1100
[cef_windows_audit_log_stopped]
search = eventtype=cef_wineventlog_security EventCode=1100
#tags = audit change endpoint stop stopped watchlist

## Audit events have been dropped by the transport.
## The security Log is now full
## The event logging service encountered an error 
## EventCodes 1101, 1104, 1108
[cef_windows_audit_errors]
search = eventtype=cef_wineventlog_security (EventCode=1101 OR EventCode=1104 OR EventCode=1108)
#tags = audit error

## The audit log was cleared
## EventCodes 1102, 517
[cef_windows_audit_log_cleared]
search = eventtype=cef_wineventlog_security (EventCode=1102 OR EventCode=517)
#tags = audit change delete endpoint cleared watchlist

## Event log automatic backup
## EventCode 1105
[cef_windows_audit_backup]
search = eventtype=cef_wineventlog_security EventCode=1105
#tags = audit backup change

## Logon/Logoff audit logs
## EventCode 4625
[cef_windows_audit_log_logon]
search = eventtype=cef_wineventlog_security EventCode=4625 
#(ta_windows_status=0xC0000064 OR ta_windows_status=0xC000006A OR ta_windows_status=0xC000006F OR ta_windows_status=0xC0000070 OR ta_windows_status=0xC0000071 OR ta_windows_status=0xC0000072 OR ta_windows_status=0XC000018C OR ta_windows_status=0XC0000192 OR ta_windows_status=0xC0000193 OR ta_windows_status=0xC0000234 OR ta_windows_status=0XC00002EE OR ta_windows_status=0XC0000413)
#tags = audit change


###### Security: Logon/Logoff ######

## User Logoff/User initiated logoff
## EventCodes 4634, 4647, 538, 551
[cef_windows_logoff]
search = eventtype=cef_wineventlog_security (EventCode=4634 OR EventCode=4647 OR EventCode=538 OR EventCode=551)
#tags = access stop logoff

## A logon was attempted using explicit credentials
## EventCodes 4648, 552
[cef_windows_logon_explicit]
search = eventtype=cef_wineventlog_security (EventCode=4648 OR EventCode=552)
#tags = authentication privileged

## An account failed to log on
## EventCodes 4625, 529, 530, 531, 532, 533, 534, 535, 536, 537, 539
[cef_windows_logon_failure]
search = eventtype=cef_wineventlog_security ((EventCode=4625 AND ta_windows_action!=error) OR EventCode=529 OR EventCode=530 OR EventCode=531 OR EventCode=532 OR EventCode=533 OR EventCode=534 OR EventCode=535 OR EventCode=536 OR EventCode=537 OR EventCode=539)
#tags = authentication

## An account was successfully logged on
## EventCodes 4624, 528, 540
[cef_windows_logon_success]
search = eventtype=cef_wineventlog_security (EventCode=4624 OR EventCode=528 OR EventCode=540)
#tags = authentication


###### Security: Object Access ######

## Object Open
## EventCodes 4656, 560
[cef_windows_object_open]
search = eventtype=cef_wineventlog_security (EventCode=4656 OR EventCode=560)
#tags = resource file access start

## Handle Closed
## EventCodes 4658, 562
[cef_windows_handle_closed]
search = eventtype=cef_wineventlog_security (EventCode=4658 OR EventCode=562)
#tags = resource file access stop


###### Security: Policy Change ######

## Audit Policy Change/The audit policy (SACL) on an object was changed
## EventCodes 4715, 4719, 612
[cef_windows_audit_policy_change]
search = eventtype=cef_wineventlog_security (EventCode=4715 OR EventCode=4719 OR EventCode=612)
#tags = policy configuration modify audit change

## System security access was granted to an account
## EventCodes 4717, 621
[cef_windows_security_access_granted]
search = eventtype=cef_wineventlog_security (EventCode=4717 OR EventCode=621)
#tags = access authorization add change account

## System security access was removed from an account
## EventCodes 4718, 622
[cef_windows_security_access_removed]
search = eventtype=cef_wineventlog_security (EventCode=4718 OR EventCode=622)
#tags = access authorization delete change account

## Per User Audit Policy was changed
## EventCodes 4912, 807
[cef_windows_audit_policy_changed]
search = eventtype=cef_wineventlog_security (EventCode=4912 OR EventCode=807)
#tags = policy configuration modify audit change

## The following policy was active when the Windows Firewall started
## EventCodes 848, 849, 850
[cef_windows_firewall_policy_active]
search = eventtype=cef_wineventlog_security (EventCode=848 OR EventCode=849 OR EventCode=850)
#tags = application firewall configuration report

## A change has been made to Windows Firewall
## EventCodes 4946, 4947, 4948, 851, 852
[cef_windows_firewall_policy_change]
search = eventtype=cef_wineventlog_security (EventCode=4946 OR EventCode=4947 OR EventCode=4948 OR EventCode=851 OR EventCode=852)
#tags = application firewall configuration modify

## The Windows Firewall has detected an application listening for incoming traffic
## EventCodes 4957, 861
[cef_windows_firewall_port_listening]
search = eventtype=cef_wineventlog_security (EventCode=4957 OR EventCode=861)
#tags = application firewall port listening report


###### Security: Privilege Use ######

## Special privileges assigned to new logon
## EventCodes 4672, 576
[cef_windows_special_privileges]
search = eventtype=cef_wineventlog_security (EventCode=4672 OR EventCode=576)
#tags = authentication privileged

## Privileged Service Called
## EventCodes 4673, 577
[cef_windows_privileged_service_call]
search = eventtype=cef_wineventlog_security (EventCode=4673 OR EventCode=577)
#tags = process execute start privileged

## Privileged object operation
## EventCodes 4674, 578
[cef_windows_privileged_object_operation]
search = eventtype=cef_wineventlog_security (EventCode=4674 OR EventCode=578)
#tags = resource execute start privileged


###### Security: Process Tracking ######

## A new process has been created
## EventCodes 4688, 592
[cef_windows_process_new]
search = eventtype=cef_wineventlog_security (EventCode=4688 OR EventCode=592)
#tags = process execute start

## A process has exited
## EventCodes 4689, 593
[cef_windows_process_exit]
search = eventtype=cef_wineventlog_security (EventCode=4689 OR EventCode=593)
#tags = process execute stop

## A process was assigned a primary token
## EventCodes 4696, 600
[cef_windows_process_token]
search = eventtype=cef_wineventlog_security (EventCode=4696 OR EventCode=600)
#tags = process execute start privileged


###### Security: System ######

## An authentication package has been loaded by the Local Security Authority
## EventCodes 4610, 514
[cef_windows_auth_package]
search = eventtype=cef_wineventlog_security (EventCode=4610 OR EventCode=514)
#tags = process execute start

## A trusted logon process has registered with the Local Security Authority
## EventCodes 4611, 515
[cef_windows_logon_process]
search = eventtype=cef_wineventlog_security (EventCode=4611 OR EventCode=515)
#tags = process authorization add

## A notification package has been loaded by the Security Account Manager
## EventCodes 4614, 518
[cef_windows_notification_package]
search = eventtype=cef_wineventlog_security (EventCode=4614 OR EventCode=518)
#tags = process execute start


###### Security:  Vulnerability ######
## System security domain policy was changed
## EventCode 4739
[cef_windows_security_misconfiguration_password_minimum_length]
search = eventtype=cef_wineventlog_security EventCode="4739" (Min__Password_Length<7 OR Mixed_Domain_Mode<7)
#tags = misconfiguration password policy vulnerability report audit change


